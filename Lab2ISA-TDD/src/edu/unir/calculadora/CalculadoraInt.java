package edu.unir.calculadora;

public interface CalculadoraInt {
	
	/**
	 * Realiza la suma de dos n�meros enteros.
	 * @param a primer n�mero.
	 * @param b segundo n�mero.
	 * @return resultado de la suma.
	 */
	public Integer suma(Integer a, Integer b);
	
	/**
	 * Realiza la resta de dos n�meros enteros.
	 * 
	 * @param a primer n�mero
	 * @param b segundo n�mero
	 * @return resultado de la resta.
	 */
	public Integer resta(Integer a, Integer b);
	
	/**
	 * Realiza la multiplicaci�n de dos n�meros enteros.
	 * 
	 * @param a multiplicando
	 * @param b multiplicador
	 * @return Producto de la multiplicaci�n.
	 */
	public Integer multiplicacion(Integer a, Integer b);
	
	/**
	 * Realiza la divisi�n de dos n�meros enteros.
	 * 
	 * @param a dividendo
	 * @param b divisor
	 * @return resultado de la divisi�n.
	 */
	public Float division(Integer a, Integer b);
	
	/**
	 * C�lculo de la ra�z cuadrada mediante el m�todo babil�nico.
	 * 
	 * @param a entero del cual deseamos calcular la ra�z cuadrada.
	 * @return ra�z cuadrada en coma flotante de precisi�n simple.
	 */
	public Float raizCuadrada(Integer a);
}
