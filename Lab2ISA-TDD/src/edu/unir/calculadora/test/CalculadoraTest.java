package edu.unir.calculadora.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

import edu.unir.calculadora.Calculadora;

class CalculadoraTest {
	private final Calculadora calculadora = new Calculadora();
	
	@Test
	void testSuma() {
		assertEquals(5, calculadora.suma(3, 2));
	}
	
	@Test
	void testSumaFloat() {
		assertEquals(1.5f, calculadora.suma(1f, 0.5f));
	}
	
	@Test
	void testResta() {
		assertEquals(1, calculadora.resta(3, 2));
		assertEquals(-1, calculadora.resta(2, 3));
		assertEquals(0, calculadora.resta(3, 3));
	}
	
	@Test
	void testRestaFloat() {
		assertEquals(0, calculadora.resta(1f, 1f));
		assertEquals(0.5f, calculadora.resta(1.5f, 1f));
		assertEquals(-0.5f, calculadora.resta(1f, 1.5f));
	}
	
	@Test
	void testMultiplicacion() {
		assertEquals(6, calculadora.multiplicacion(3, 2));
	}
	
	@Test
	void testMultiplicacionFloat() {
		assertEquals(6f, calculadora.multiplicacion(3f, 2f));
	}
	
	@Test
	void testDivision() {
		assertEquals(2, calculadora.division(4, 2));
	}
	
	@Test
	void testDivisionFloat() {
		assertEquals(0.5f, calculadora.division(1f, 2f));
	}
	
	@Test
	void testDivisionPorCero() {
		assertEquals("Infinity", calculadora.division(2, 0).toString());
	}
	
	@Test
	void testPrecisionDivision() {
		assertTrue(calculadora.getPrecision(calculadora.division(1, 3)) >= 5);
	}
	
	@Test
	void testRaiz() {
		assertEquals(1.4142157f, calculadora.raizCuadrada(2));
	}
	
	@Test
	void testRaizNegativa() {
		assertEquals(null, calculadora.raizCuadrada(-1));
	}

	@Test
	void testFactorizar() {
		Integer[] factores = calculadora.factorizar(18);
		assertEquals(6, factores[0]);
		assertEquals(3, factores[1]);
	}
	
	@Test
	void testAbs() {
		assertEquals(1, calculadora.abs(-1f));
	}
	
	@Test
	void testPrecision() {
		assertEquals(5, calculadora.getPrecision(0.00001f));
	}
}
