package edu.unir.calculadora;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Calculadora implements CalculadoraInt {
	
	@Override
	public Integer suma(Integer a, Integer b) {
		return new Integer(a + b);
	}
	
	public Float suma(Float a, Float b) {
		return new Float(a + b);
	}

	@Override
	public Integer resta(Integer a, Integer b) {
		return new Integer(a - b);
	}
	
	public Float resta(Float a, Float b) {
		return new Float(a - b);
	}

	@Override
	public Integer multiplicacion(Integer a, Integer b) {
		return new Integer(a * b);
	}
	
	public Float multiplicacion(Float a, Float b) {
		return new Float(a * b);
	}
	
	public Float division(Float a, Float b) {
		return new Float(a / b);
	}

	@Override
	public Float division(Integer a, Integer b) {
		return new Float((float)a / (float)b);
	}

	@Override
	public Float raizCuadrada(Integer a) {
		if(a <= 0) {
			if(a < 0) {
				return null;
			}
			return 0f;
		}
		
		Integer[] factores = factorizar(a);
		
		Float h = new Float(factores[0]);
		Float b = new Float(factores[1]);
		
		while(abs(resta(h, b)) > 1E-5) {
			b = division(suma(h, b), 2f);
			h = division(a.floatValue(), b);
		}
		
		return b;
	}
	
	/**
	* Devuelve un n�mero con la precisi�n de la parte decimal de
	* un n�mero en coma flotante de precisi�n simple. Esta operaci�n se realiza evaluando la 
	* parte del n�mero decimal y devolviendo su longitud (0 si el n�mero �nicamente tiene
	* parte entera).
	* 
	* @param  numero el n�mero flotante cuya parte decimal queremos evaluar.
	* @return un valor entero con la precisi�n de su parte decimal.
	*/
	public Integer getPrecision(Float numero) {
		String decimal = numero.toString().split(Pattern.quote("."))[1];

		if(decimal.indexOf('E') == -1) {
			return (decimal.length() == 1 && decimal.charAt(0) == '0') ? 0 : decimal.length();
		}
		
		return new Integer(decimal.split(Pattern.quote("E-"))[1]);
	}
	
	/**
	 * Descompone un n�mero en dos factores. Esta funci�n se utiliza para
	 * el c�lculo de la ra�z cuadrada por el m�todo babil�nico.
	 * 
	 * @param num el n�mero que queremos descomponer en dos factores.
	 * @return un array de Integer que contiene dos elementos.
	 */
	public Integer[] factorizar(Integer num) {
		Integer[] res = new Integer[2];
		List<Integer> factores = new ArrayList<Integer>();
		
		int divisor = 2;
		
		res[0] = 1;
		res[1] = 1;
		factores.add(1);
		
		if(num != 1) {
			while (num != 1) {
				if(num % divisor == 0) {
					factores.add(divisor);
					num /= divisor;
				} else {
					divisor++;
				}
			}
			
			for(int i = 0; i<factores.size()-1; i++) {
				res[0] *= factores.get(i);
			}
			res[1] = factores.get(factores.size()-1);
		}
		
		return res;
	}
	
	/**
	 * Devuelve el valor absoluto de un n�mero
	 * 
	 * @param num el n�mero que puede ser positivo o negativo.
	 * @return el valor absoluto de dicho n�mero.
	 */
	public Float abs(Float num) {
		if(num < 0) {
			return -num;
		}
		
		return num;
	}
}
