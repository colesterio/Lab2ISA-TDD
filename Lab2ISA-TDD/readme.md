# Laboratorio 2 - Ingenier�a del Software Avanzada

En este proyecto se implementar� mediante la t�cnica de desarrollo TDD (Test Driven Development, o desarrollo dirigido por pruebas) 
una calculadora simple que contendr� la siguiente funcionalidad:

- Suma y resta de n�meros enteros.
- Multiplicaci�n y divisi�n de n�meros enteros.
- Ra�z cuadrada de n�meros enteros.

Para implementarla no se podr� hacer uso de ninguna librer�a matem�tica y se deber�n utilizar operaciones b�sicas como la suma, resta, 
multiplicaci�n y divisi�n, empleando para ello las funciones de la propia calculadora implementadas en los dos pasos anteriores. 
La precisi�n alcanzada en los c�lculos debe ser superior a 10-5.

## Autor

Fernando Hidalgo Paredes

### �Qu� es TDD?

Es una metodolog�a de desarrollo que, a grandes rasgos, comprende un proceso c�clico compuesto por tres partes:

1. Escribir un test que falle (puesto que no hay funcionalidad implementada asociada a dicho test).

2. Implementar la funcionalidad m�nima para que pase el test.

3. Refactorizar el c�digo para mejorarlo.