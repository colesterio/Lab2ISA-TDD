# Laboratorio 2 - Ingeniería del Software Avanzada

En este proyecto se implementará mediante la técnica de desarrollo TDD (Test Driven Development, o desarrollo dirigido por pruebas) 
una calculadora simple que contendrá la siguiente funcionalidad:

- Suma y resta de números enteros.
- Multiplicación y división de números enteros.
- Raíz cuadrada de números enteros.

Para implementarla no se podrá hacer uso de ninguna librería matemática y se deberán utilizar operaciones básicas como la suma, resta, 
multiplicación y división, empleando para ello las funciones de la propia calculadora implementadas en los dos pasos anteriores. 
La precisión alcanzada en los cálculos debe ser superior a 10-5.

## Autor

Fernando Hidalgo Paredes

### ¿Qué es TDD?

Es una metodología de desarrollo que, a grandes rasgos, comprende un proceso cíclico compuesto por tres partes:

1. Escribir un test que falle (puesto que no hay funcionalidad implementada asociada a dicho test).

2. Implementar la funcionalidad mínima para que pase el test.

3. Refactorizar el código para mejorarlo.